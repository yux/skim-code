# skim code
Cut.C file is a script to skim over the ntuple files with different lepton and photon requirement
4 functions
lep1ph1(): Require at least 1 lepton and at least 1 photon
lep1ph2(): Require at least 1 lepton and at least 2 photon
lep2ph1(): Require at least 2 lepton and at least 1 photon
lep2ph2(): Require at least 2 lepton and at least 2 photon
